#include <stdio.h>

void heapify(int arr[],int i,int n){
	int p1=i<<1, p2=((i<<1)|1);

	if(p1>n)
		return;
	
	if(p1<=n && p2<=n)
	{
		int tmp = arr[i];
		if(arr[p1]>tmp && arr[p2]>tmp)
		{
			if(arr[p1]>arr[p2])
			{
				arr[i]=arr[p1];
				arr[p1]=tmp;
				heapify(arr,p1,n);
			}
			else
			{
				arr[i]=arr[p2];
				arr[p2]=tmp;
				heapify(arr,p2,n);
			}
		}
		else if(arr[p1]>tmp)
		{
			arr[i]=arr[p1];
			arr[p1]=tmp;
			heapify(arr,p1,n);
		}
		else if(arr[p2]>tmp)
		{
			arr[i]=arr[p2];
			arr[p2]=tmp;
			heapify(arr,p2,n);
		}
	}
	else if(arr[p1]>arr[i])
		{
			int tmp=arr[i];
			arr[i]=arr[p1];
			arr[p1]=tmp;
			heapify(arr,p1,n);
		}
	return;
}
void Buildheap(int arr[],int n){
	int i;
	for(i=n;i>0;i--)
		heapify(arr,i,n);
	return;
}
void insert(int arr[] , int j)
{
	int i=j/2;
	while(i>0 && arr[i]<arr[j])
	{
	//	printf("arr[%d]=%d arr[%d]=%d\n",i,arr[i],j,arr[j]);
		int tmp = arr[j];
		arr[j]=arr[i];
		arr[i]=tmp;
		j=i;
		i=i/2;
	}
	return;
}
void delete(int arr[],int x,int n){
	int i;
	for(i=1;i<n;i++)
	{
		if(arr[i]==x)
		{
			arr[i]=arr[n];
			if(i/2!=0 && arr[i]>arr[i/2])
				insert(arr,i);
			else
				heapify(arr,i,n-1);
			break;
		}
	}
	return;
}
void sort(int arr[],int n){

	int i,tmp;
	for(i=n;i>0;i--)
	{
		tmp=arr[1];
		arr[1]=arr[i];
		arr[i]=tmp;
		heapify(arr,1,i-1);
	}
	return;
}
void print(int arr[] , int n){
	int i;
	for(i=1;i<=n;i++)
		printf("%d ",arr[i]);
	return;
}
int main(){
	int n,i;
	scanf("%d",&n);
	int arr[10000];

	for(i=1;i<n+1;i++)
		scanf("%d",&arr[i]);

	Buildheap(arr,n);
	print(arr,n);
	printf("\n");
	int x;
	scanf("%d",&x);
	n++;
	arr[n]=x;
	insert(arr,n);
	print(arr,n);
	printf("\n");
	scanf("%d",&x);
	delete(arr,x,n);
	n--;
	print(arr,n);
	printf("\n");
	sort(arr,n);
	print(arr,n);
	printf("\n");

	return 0;
}
