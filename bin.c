#include <stdio.h>
#include <stdlib.h>

typedef struct node
{
	int val ;
	struct node *left, *right ;
}node ;

node* nnode(void)
{
}
node* insert(node* cur,int val)
{
	if(cur==NULL)
	{
		cur=(node*)malloc(sizeof(node));
		cur->left=NULL;
		cur->right=NULL;
		cur->val=val;
		return cur;
	}

	if(val == cur->val) return cur ;
	if(val<cur->val) 
		cur->left = insert(cur->left,val) ;
	else 
		cur->right = insert(cur->right,val) ;
	return cur;
}
node* min(node *head)
{
	while(head->left!=NULL)
		head=head->left;

	return head;
}
node* max(node *head)
{
	while(head->right!=NULL)
		head=head->right;

	return head;
}
int search(node *cur,int val)
{
	int ans=-1;
	if(cur==NULL)
		return -1;
	if(cur->val==val)
		return 1;
	if(val<cur->val)
		return search(cur->left,val) ;
	else
		return search(cur->right,val) ;
}
node* delete(node* cur , int val)
{
	node *temp=cur;
	if(cur==NULL)
		return cur;
	if(cur->val==val)
	{
		if(cur->left==NULL && cur->right==NULL)
		{
			free(cur);
			return NULL;
		}
		else if(cur->left==NULL)
		{
			cur=cur->right;
			free(temp);
		}
		else if(cur->right==NULL)
		{
			cur=cur->left;
			free(temp);
		}
		else
		{
			node *temp1=max(cur->left);
			int x=temp1->val;
			delete(cur , temp1->val);
			cur->val=x;
		}
		return cur;
	}
	if(val<cur->val)
		//	if(cur!=NULL)
		cur->left=delete(cur->left,val) ;
	else
		//	if(cur!=NULL)
		cur->right=delete(cur->right,val) ;

	return cur;
}
node* successor(node *cur,int val)
{
	if(cur==NULL);
	return NULL;

	if(cur->val>val)
	{
		node* ret = successor(cur->left,val);
		return ret == NULL ? cur : ret;
	}	
	else return successor(cur->right,val);
}
void inf(node *cur)
{
	if(cur==NULL)
		return;
	inf(cur->left);
	printf("%d ",cur->val);
	inf(cur->right);
	return;
}
void pof(node *cur)
{
        if(cur==NULL)
                return;
        pof(cur->left);
	pof(cur->right);
        printf("%d ",cur->val);
//        inf(cur->right);
        return;
}
void pref(node *cur)
{
        if(cur==NULL)
                return;
        printf("%d ",cur->val);
        pref(cur->left);
        pref(cur->right);
//        inf(cur->right);
        return;
}
int main()
{
	int n,i,x;
	scanf("%d",&n);
	node *root=NULL;
	for(i=0;i<n;i++)
	{
		scanf("%d",&x);
		root=insert(root,x);
	}
	pref(root);
	printf("\n");
	pof(root);
	printf("\n");
	//printf("%d",max(root)->val);
	inf(root);
	printf("\n");
	scanf("%d",&n);
	delete(root,n);
	//printf("%d",delete(root,n));
	inf(root);
	return 0;
}
