#include <stdio.h>
#include <stdlib.h>

typedef struct node
{
	int val ;
	struct node *left, *right ;
}node ;

node* nnode(void)
{
	node* temp = (node*)malloc(sizeof(node)) ;
	temp->left = temp->right = NULL ;
	temp->val = -1 ;
	return temp ;
}
node* insert(node* cur,int val)
{
	if(cur == NULL)
	{
		cur = nnode() ;
		cur->val = val ;
		return cur ;
	}
	if(val == cur->val) return cur ;
	if(val<cur->val) cur->left = insert(cur->left,val) ;
	else cur->right = insert(cur->right,val) ;
	return cur ;
}
node* min(node *head)
{
	while(head->left !=NULL) head = head->left ;
	return head ;
}
node* max(node *head)
{
	while(head->right!=NULL) head = head->right ;
	return head ;
}

int search(node *cur,int val)
{
	if(cur == NULL) return -1 ;
	if(cur->val == val) return 1 ;
	if(cur->val < val) return search(cur->right,val) ;
	return search(cur->left,val) ;
}
node* delete(node* cur,int val)
{
	if(cur == NULL) return NULL ;
	if(cur->val == val)
	{
		if(cur->right != NULL &&
				cur->left != NULL)
		{
			node* temp = cur->right ;
			while(temp->left != NULL) temp = temp->left ;
			cur->val = temp->val ;
			cur->right = delete(cur->right,temp->val) ;
			return cur ;
		}
		node* temp = cur->right != NULL ? cur->right : cur->left ;
		free(cur) ;
		return temp ;
	}
	if(cur->val < val) cur->right = delete(cur->right,val) ;
	else cur->left = delete(cur->left,val) ;
	return cur ;
}
